#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    return newBoard;
}

Move* Board::intToMove(int location) {
    int x = location % 8;
    int y = location / 8;
    Move * move = new Move(x, y);
    return move;
}
/*
vector<Move*> Board::getNeighbors(int location, Side s) {
    // Check the location for adjacent opponents pieces
    vector<Move*> neighbors;
    bool up = true;
    bool down = true;
    bool left = true;
    bool right = true;
    int[] rightEdge = {7, 15, 23, 31, 39, 47, 55, 63};
    for (int i = 0; i < 8; i++)
    {  
        if (location == rightEdge[i])
            right = false;
    }
    if (location % 8 == 0) 
        left = false;
    if (location < 8)
        up = false;
    else if (location > 55)
        down = false;
    
    if (right)
    {
        if (taken[location + 1])
        {
            if (black[location + 1])    
            {
                if (s == WHITE)
                    neighbors.push_back(intToMove(location + 1));
            }
            else
            {
                if (s == BLACK)
                    neighbors.push_back(intToMove(location + 1));
            }
        }
    }
    if (left)
    {
        if (taken[location - 1])
        {
            if (black[location - 1])    
            {
                if (s == WHITE)
                    neighbors.push_back(intToMove(location - 1));
            }
            else
            {
                if (s == BLACK)
                    neighbors.push_back(intToMove(location - 1));
            }
        }
    }
    if (up)
    {
        if (taken[location - 8])
        {
            if (black[location - 8])    
            {
                if (s == WHITE)
                    neighbors.push_back(intToMove(location - 8));
            }
            else
            {
                if (s == BLACK)
                    neighbors.push_back(intToMove(location - 8));
            }
        }
    }
    if (down)
    {
        if (taken[location + 8])
        {
            if (black[location + 8])    
            {
                if (s == WHITE)
                    neighbors.push_back(intToMove(location + 8));
            }
            else
            {
                if (s == BLACK)
                    neighbors.push_back(intToMove(location + 8));
            }
        }
    }
    return neighbors;
}*/
        
bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
