#include "player.h"
#include <vector>
#include <iostream>
#include <limits>
#include <cstdio>

using namespace std;
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side color) {
    // Will be set to true in test_minimax.cpp.
    board = Board();
    testingMinimax = false;
    me = color;
    if (me == WHITE)
    {
        cerr << "WHITE" << endl;
        him = BLACK;
    }
    else
    {
        cerr << "BLACK" << endl;
        him = WHITE;
    }
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */
    if (opponentsMove != NULL)
        board.doMove(opponentsMove, him);
    if (board.hasMoves(me) == false)
        return NULL;
    vector<Move*> possibleMoves;
    for (int i = 0; i < 64; i++) 
    {// Go through the entire board to find the possible moves
        if (board.checkMove(board.intToMove(i), me))
        //Add the available spaces to the vector of all possible moves
            possibleMoves.push_back(board.intToMove(i));
    }
    int bestS = (-1) * numeric_limits<int>::max();// initialize best score to be -Infinity
    Move* bestM = possibleMoves[0];// initialze best move to be first one
    for (int i = 0; i < (int) possibleMoves.size(); i++)
    {
        bool containsCorners = false;
        bool containsNear = false;
        vector<Move*> corners;
        vector<Move*> nearCorners;
        Move *topLeft = new Move(0, 0);
        Move *topRight = new Move(7, 0);
        Move *bottomLeft = new Move(0, 7);
        Move *bottomRight = new Move(7, 7);
        Move *near1 = new Move(0, 1);
        Move *near2 = new Move(1, 0);
        Move *near3 = new Move(1, 1);
        Move *near4 = new Move(6, 0);
        Move *near5 = new Move(7, 1);
        Move *near6 = new Move(6, 1);
        Move *near7 = new Move(0, 6);
        Move *near8 = new Move(1, 7);
        Move *near9 = new Move(1, 6);
        Move *near10 = new Move(6, 7);
        Move *near11 = new Move(7, 6);
        Move *near12 = new Move(6, 6);
        
        corners.push_back(topLeft);
        corners.push_back(topRight);
        corners.push_back(bottomLeft);
        corners.push_back(bottomRight);
    
        nearCorners.push_back(near1);
        nearCorners.push_back(near2);
        nearCorners.push_back(near3);
        nearCorners.push_back(near4);
        nearCorners.push_back(near5);
        nearCorners.push_back(near6);
        nearCorners.push_back(near7);
        nearCorners.push_back(near8);
        nearCorners.push_back(near9);
        nearCorners.push_back(near10);
        nearCorners.push_back(near11);
        nearCorners.push_back(near12);
        
        Board *test = board.copy();//copy the board
        Move *curr = possibleMoves[i];
        test->doMove(curr, me);//test out the move
        int score = test->countBlack() - test->countWhite(); // calculate the score
        if (me == WHITE)
            score *= -1;
        for (int j = 0; j < (int) corners.size(); j++)
        {
            if ((corners[j]->getX() == curr->getX()) && (corners[j]->getY() == curr->getY()))
                containsCorners = true;
            else if ((nearCorners[j]->getX() == curr->getX()) && (nearCorners[j]->getY() == curr->getY()))
                containsNear = true;
        }
        if (containsCorners)
            score *= 200;
        else if (containsNear)
            score *= -200;
        // LAYER 2
        vector<Move*> possibleMoves2;
        for (int j = 0; j < 64; j++) 
        {// Go through the entire board to find the possible moves for my opponent
            if (test->checkMove(test->intToMove(j), him))// changed i to j
            //Add the available spaces to the vector of all possible moves
                possibleMoves2.push_back(test->intToMove(j));
        }    
        int score2;
        for (int j = 0; j < (int) possibleMoves2.size(); j++)
        {
            bool containsCorners2 = false;
            bool containsNear2 = false;
            Board *test2 = test->copy();//copy the board
            Move *curr2 = possibleMoves2[j];
            test2->doMove(curr2, him);//test out the move
            score2 = test2->countBlack() - test2->countWhite();
            if (me == WHITE)
                score2 *= -1;
            for (int k = 0; k < (int) corners.size(); k++)
            {
                if ((corners[k]->getX() == curr->getX()) && (corners[k]->getY() == curr->getY()))
                    containsCorners2 = true;
                else if ((nearCorners[k]->getX() == curr->getX()) && (nearCorners[k]->getY() == curr->getY()))
                    containsNear2 = true;
            }
            if (containsCorners2)
                score2 *= 200;
            else if (containsNear2)
                score2 *= -200;
            // LAYER 3
            vector<Move*> possibleMoves3;
            for (int n = 0; n < 64; n++) 
            {// Go through the entire board to find the possible moves for me again
                if (test->checkMove(test->intToMove(n), him))
                //Add the available spaces to the vector of all possible moves
                    possibleMoves3.push_back(test->intToMove(n));
            }    
            int score3;
            for (int k = 0; k < (int) possibleMoves3.size(); k++)
            {
                bool containsCorners3 = false;
                bool containsNear3 = false;
                Board *test3 = test2->copy();
                Move *curr3 = possibleMoves3[k];
                test3->doMove(curr3, me);
                score3 = test3->countBlack() - test3->countWhite();
                if (me == WHITE)
                    score3 *= -1;
                for (int n = 0; n < (int) corners.size(); n++)
                {
                    if ((corners[n]->getX() == curr->getX()) && (corners[n]->getY() == curr->getY()))
                        containsCorners3 = true;
                    else if ((nearCorners[n]->getX() == curr->getX()) && (nearCorners[n]->getY() == curr->getY()))
                        containsNear3 = true;
                }
                if (containsCorners3)
                    score3 *= 200;
                else if (containsNear3)
                    score3 *= -200;
            }
        }
        int score3 = score2 + score;
        if (score3 > bestS)
        {
            bestS = score3;
            bestM = possibleMoves[i];
        }
        /*  FIGURE OUT HOW TO FREE MEMORY
         * if (i == ((int) possibleMoves.size() - 1))
        {
            delete test;
            delete curr;
            for (int i = 0; i < 12; i++)
            {
                if (i < 5)
                    delete corners[i];
                delete nearCorners[i];
            }
        }*/
    }
    board.doMove(bestM, me);
    //delete bestM;
    return bestM;
}
