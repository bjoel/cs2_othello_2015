For assignment 10, I changed my player to go three layers deep instead of two.
This means, for a given board, it checks every possible move, then checks what
my opponent could do if I make those moves, then looks even further ahead to
see what my I could do after my opponent makes his best move given my first
one.  I make the move that will give me the best option on my next turn if my
opponent plays his best move.    

My heuristic used to be the "score" meaning total # of my pieces minus the total
# of my opponents pieces.  I multiplied the score by 5 for a move if the move was
a corner.  I multiplied it by -5 if the move was next to a corner meaning my opponent
had the option of taking the corner.  

THIS WEEK:  I changed it so that the corner and near corner multipliers to 500 and
-500.  This way, I effectively guarantee that I'll make the move to a corner if it is
a possible move and that I, no matter what, avoid the moves next to corners.
I found that controlling the corners is essential so that is why I did just a heavy
multiplier for the corners.